
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define M 2                    /* number of op's in table     */
#define NA 516                 /* number of shooting points   */
#define NB 388

/*************************************************************************/
/***********************  Function Declarations  *************************/
/*************************************************************************/

/*************************************************************************/
/********************************  MAIN  *********************************/
/*************************************************************************/

int main(){

  int i, j, k, l, m, imax, kmax, lmax;
  double qA[1+M][1+NA], qB[1+M][1+NB];
  double zA[1+M][1+NA], zB[1+M][1+NB];
  double qmax[1+M], qmin[1+M], qspan[1+M];
  double qmaxA[1+M], qmaxB[1+M], qminA[1+M], qminB[1+M];
  double rc, alpha[1+M], lnL, temp;
  double best1[1+M][1+1+1], best2[1+M][1+M][1+2+1];
  double best3[1+M][1+M][1+M][1+3+1];
  FILE *list, *zlist, *answer, *data;

  system("pwd");
  system("date");
  system("colrm 1 5 < A-list.txt > A.txt");
  system("colrm 1 5 < B-list.txt > B.txt");
  /* READ DATA FILES FROM AIMLESS SHOOTING */
  data = fopen("A.txt","r");
  for(i=1;i<=NA;i=i+1){
    for(j=1;j<=M;j=j+1){
      fscanf(data, "%lf ", &qA[j][i]);
    }
  }
  fclose(data);
  data = fopen("B.txt","r");
  for(i=1;i<=NB;i=i+1){
    for(j=1;j<=M;j=j+1){
      fscanf(data, "%lf ", &qB[j][i]);
    }
  }
  fclose(data);

  printf("\n  SCANNED INPUT");
  for(i=1;i<=NA;i=i+1){
    qA[1][i] = sqrt(qA[1][i]/(4.0*3.1416));
    qA[2][i] = qA[2][i]/(2.0*3.1416);
  }
  for(i=1;i<=NB;i=i+1){
    qB[1][i] = sqrt(qB[1][i]/(4.0*3.1416));
    qB[2][i] = qB[2][i]/(2.0*3.1416);
  }

  for(i=1;i<=M;i=i+1){
    qminA[i] = qA[i][1];
    for(j=1;j<=NA;j=j+1){
      if(qA[i][j] < qminA[i]){
        qminA[i] = qA[i][j];
      }
    }
    qmaxA[i] = qA[i][1];
    for(j=1;j<=NA;j=j+1){
      if(qA[i][j] > qmaxA[i]){
	qmaxA[i] = qA[i][j];
      }
    }
    qminB[i] = qB[i][1];
    for(j=1;j<=NB;j=j+1){
      if(qB[i][j] < qminB[i]){
        qminB[i] = qB[i][j];
      }
    }
    qmaxB[i] = qB[i][1];
    for(j=1;j<=NB;j=j+1){
      if(qB[i][j] > qmaxB[i]){
        qmaxB[i] = qB[i][j];
      }
    }	
  }

  for(i=1;i<=M;i=i+1){
    if(qmaxA[i] > qmaxB[i]){
      qmax[i] = qmaxA[i];
    }else{
      qmax[i] = qmaxB[i];
    }
    if(qminA[i] < qminB[i]){
      qmin[i] = qminA[i];
    }else{
      qmin[i] = qminB[i];
    }
    qspan[i] = qmax[i] - qmin[i];
    printf("\n %d %.3f %.3f %.3f ", i, qmin[i], qmax[i], qspan[i]); 
  }

  printf("\n  REDUCED VARIABLES: Z = (Q-Qmin)/(Qmax-Qmin) \n");
  printf("\n        Z IN [0, 1]: Q = Z(Qmax-Qmin)+Qmin\n"); 
  for(i=1;i<=M;i=i+1){
    for(j=1;j<=NA;j=j+1){
      zA[i][j] = (qA[i][j]-qmin[i])/(qmax[i]-qmin[i]);
    }
    for(j=1;j<=NB;j=j+1){
      zB[i][j] = (qB[i][j]-qmin[i])/(qmax[i]-qmin[i]);
    }
  }
  fflush(stdout);
  
  list = fopen("A.txt","w");
  fprintf(list, "%d\n", NA);
  for(j=1;j<=NA;j=j+1){
    fprintf(list, "\n");
    for(i=1;i<=M;i=i+1){
      fprintf(list, "%f ", zA[i][j]);
    }
  }
  fclose(list);

  list = fopen("B.txt","w");
  fprintf(list, "%d\n", NB);
  for(j=1;j<=NB;j=j+1){
    fprintf(list, "\n");
    for(i=1;i<=M;i=i+1){
      fprintf(list, "%f ", zB[i][j]);
    }
  }
  fclose(list);

  printf("\n\n  BIC = LN(N)/2 = %.3f\n", 0.5*log((double)(NA+NB))); 



  printf("\n  PREPARE AND COMPILE MAXIMIZATION ROUTINE FOR M=1"); 
  m = 1;
  list = fopen("globals.txt","w");
  fprintf(list, "\n#define A %d", NA);
  fprintf(list, "\n#define B %d", NB);
  fprintf(list, "\n#define m %d\n", m);
  fclose(list);

  system("gcc maximizePBnew.c -lm");
  system("cp a.out max.exe");

  for(i=1;i<=M;i=i+1){
    zlist = fopen("zlist.txt", "w");
    fprintf(zlist, "\n %d %d\n", NA, NB);
    for(j=1;j<=NA;j=j+1){
      fprintf(zlist, "\n %f ", zA[i][j]);
    }
    for(j=1;j<=NB;j=j+1){
      fprintf(zlist, "\n %f ", zB[i][j]);
    }
    fclose(zlist);
    system("./max.exe > a1.txt");
    fflush(stdout);
    answer = fopen("answer.txt", "r");
    fscanf(answer, "%lf ", &temp);
    lnL = temp;
    best1[i][m+1] = lnL;
    if(i==1){
      system("cp zlist.txt z-1.txt");
      imax = 1;
    }else{
      if(best1[i][m+1] > best1[imax][m+1]){
	system("cp zlist.txt z-1.txt");
	imax = i;
      }
    }
    printf("\n rxncoor %d %f", i, lnL);
    for(j=0;j<=m;j=j+1){
      fscanf(answer, "%lf ", &temp);
      alpha[j] = temp;
      printf(" %.3f", alpha[j]);
      best1[i][j] = alpha[j];
    }
    fclose(answer);
  }
  printf("\n ");
  printf("\n best rxncoor %d %f", imax, best1[imax][m+1]);
  for(j=0;j<=m;j=j+1){
    printf(" %.3f", best1[imax][j]);
  }
  
  sleep(1);
  if(M > 1){
    printf("\n\n PREPARE AND COMPILE MAXIMIZATION ROUTINE ");
    m = 2;
    list = fopen("globals.txt","w");
    fprintf(list, "\n#define A %d", NA);
    fprintf(list, "\n#define B %d", NB);
    fprintf(list, "\n#define m %d\n", m);
    fclose(list);
    system("gcc maximizePBnew.c -lm");
    system("cp a.out max.exe");

    for(i=1;i<=M;i=i+1){
      for(k=i+1;k<=M;k=k+1){
	zlist = fopen("zlist.txt", "w");
	fprintf(zlist, "\n %d %d\n", NA, NB);
	for(j=1;j<=NA;j=j+1){
	  fprintf(zlist, "\n %f %f", zA[i][j], zA[k][j]);
	}
	for(j=1;j<=NB;j=j+1){
	  fprintf(zlist, "\n %f %f", zB[i][j], zB[k][j]);
	}
	fclose(zlist);
	list = fopen("initial.txt", "w");
	if(best1[i][2] > best1[k][2]){
	  fprintf(list, "%f %f %f %f ", 
		  best1[i][0], best1[i][1], 0.0, best1[i][2]); 
	}else{
	  fprintf(list, "%f %f %f %f ",
                  best1[k][0], 0.0, best1[k][1], best1[k][2]); 
	}
	fclose(list);
	system("./max.exe > a1.txt");
	fflush(stdout);
	answer = fopen("answer.txt", "r");
	fscanf(answer, "%lf ", &temp);
	lnL = temp;
	best2[i][k][m+1] = lnL;
	if(i==1  &&  k==2){
	  system("cp zlist.txt z-2.txt");
	  imax = 1;
	  kmax = 2;
	}else{
	  if(best2[i][k][m+1] > best2[imax][kmax][m+1]){
	    system("cp zlist.txt z-2.txt");
	    imax = i;
	    kmax = k;
	  }
	}
	printf("\n rxncoor %d %d %f", i, k, lnL);
	for(j=0;j<=m;j=j+1){
	  fscanf(answer, "%lf ", &temp);
	  alpha[j] = temp;
	  printf(" %.3f", alpha[j]);
	  best2[i][k][j] = alpha[j];
	}
	fclose(answer);
      }
    }
  }
  printf("\n ");
  printf("\n best rxncoor %d %d %f", imax, kmax, best2[imax][kmax][m+1]);
  for(j=0;j<=m;j=j+1){
    printf(" %.3f", best2[imax][kmax][j]);
  }

  /*
  sleep(1);
  fflush(stdout);
  if(M > 2){
    printf("\n\n PREPARE AND COMPILE MAXIMIZATION ROUTINE ");
    m = 3;
    list = fopen("globals.txt","w");
    fprintf(list, "\n#define A %d", alist[0]);
    fprintf(list, "\n#define B %d", blist[0]);
    fprintf(list, "\n#define m %d\n", m);
    fclose(list);
    system("gcc -lm maximizePBnew.c");
    system("cp a.out max.exe");
    for(i=1;i<=M;i=i+1){
      for(k=i+1;k<=M;k=k+1){
	for(l=k+1;l<=M;l=l+1){
	  zlist = fopen("zlist.txt", "w");
	  fprintf(zlist, "\n %d %d\n", alist[0], blist[0]);
	  for(j=1;j<=alist[0];j=j+1){
	    fprintf(zlist, "\n %f %f %f", z[i][alist[j]], z[k][alist[j]], z[l][alist[j]]);
	  }
	  for(j=1;j<=blist[0];j=j+1){
	    fprintf(zlist, "\n %f %f %f", z[i][blist[j]], z[k][blist[j]], z[l][blist[j]]);
	  }
	  fclose(zlist);

	  list = fopen("initial.txt", "w");
	  if( (best2[i][k][3] > best2[k][l][3]) && (best2[i][k][3] > best2[i][l][3]) ){
	    fprintf(list, "%f %f %f %f %f ",
		    best2[i][k][0], best2[i][k][1], best2[i][k][2], 0.0, best2[i][k][3]);
	  }else{
	    if( (best2[i][l][3] > best2[k][l][3]) && (best2[i][l][3] > best2[i][k][3]) ){
	      fprintf(list, "%f %f %f %f %f ",
		      best2[i][l][0], best2[i][l][1], 0.0, best2[i][l][2], best2[i][l][3]);
	    }else{
              fprintf(list, "%f %f %f %f %f ",
                      best2[k][l][0], 0.0, best2[k][l][1], best2[k][l][2], best2[k][l][3]);
	    }
	  }
	  fclose(list);

	  system("./max.exe > a1.txt");
	  fflush(stdout);
	  answer = fopen("answer.txt", "r");
	  fscanf(answer, "%lf ", &temp);
	  lnL = temp;

	  best3[i][k][l][m+1] = lnL;
	  if(i==1  &&  k==2  &&  l==3){
	    system("cp zlist.txt z-3.txt");
	    imax = 1;
	    kmax = 2;
	    lmax = 3;
	  }else{
	    if(best3[i][k][l][m+1] > best3[imax][kmax][lmax][m+1]){
	      system("cp zlist.txt z-3.txt");
	      imax = i;
	      kmax = k;
	      lmax = l;
	    }
	  }

	  printf("\n rxncoor %d %d %d %f", i, k, l, lnL);
	  for(j=0;j<=m;j=j+1){
	    fscanf(answer, "%lf ", &temp);
	    alpha[j] = temp;
	    best3[i][k][l][j] = temp;
	    printf(" %.3f", alpha[j]);
	  }
	  fclose(answer);
	}
      }
    }
    printf("\n ");
    printf("\n best rxncoor %d %d %d %f", imax, kmax, lmax, best3[imax][kmax][lmax][m+1]);
    for(j=0;j<=m;j=j+1){
      printf(" %.3f", best3[imax][kmax][lmax][j]);
    }
  }
  */

  printf("\n");
}

