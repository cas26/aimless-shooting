  
int mod(int);
void copy_state(int [N][N], int [N][N]);
int randomint(int, int);
float ran01(void);
float energy(float, float, int [N][N]);
float metropolis(float);
int metropolis_mc(float);
void print_state(FILE *, int [N][N]);
float e_change(float, float, int, int, int [N][N]);
int element_of_cluster(int, int, int [1+N*N][3]);
void find_big_cluster(int [N][N], int [1+N*N][3]);
void reset_cluster(int [1+N*N][3]);
int perimeter(int [N][N], int [1+N*N][3]);
void cluster_from_cluster(int [N][N], int [1+N*N][3]);
void edge_of_cluster(int [1+N*N][3], int [1+N*N][3]);
void biased_mc_step(float, float, int [N][N], 
		    int [1+N*N][3], int [1+N*N][3]);
float rxnc(int, int);
void copy_cluster(int [1+N*N][3], int [1+N*N][3]);
void traj_segment(float, float, int,
		  int [N][N], int [N][N]);
float magnetization(int [N][N]);
char hfn(int, int);


char hfn(int n, int s){
  char c;
  float r;
  r = rxnc(n, s);
  if(r < redgeA){
    c = 'A';
  }else{
    if(r > redgeB){
      c = 'B';
    }else{
      c = 'I';
    }
  }
  return c;
}


float magnetization(int state[N][N]){
  int i, j, nets;
  float m;
  nets = 0;
  for(i=0;i<N;i=i+1){
    for(j=0;j<N;j=j+1){
      nets = nets + state[i][j];
    }
  }
  m = ((float)nets);
  return m;
}

void traj_segment(float hh, float JJ, int tmax,
		  int start[N][N], int state[N][N]){
  int i, j, k, accept, t;
  float E, dE;
  copy_state(start, state);
  E = energy(hh, JJ, state);
  for(t=1;t<=tmax;t=t+1){
    i = randomint(0, N-1);
    j = randomint(0, N-1);
    dE = e_change(hh, JJ, i, j, state);
    accept = metropolis_mc(dE);
    if(accept == 1){
      E = E + dE;
      state[i][j] = -state[i][j];
    }
  }
}

void print_state(FILE *shoots, int state[N][N]){
  int i, j;
  fprintf(shoots, "\n ");
  for(i=0;i<=(N-1);i=i+1){
    fprintf(shoots, "\n ");
    for(j=0;j<=(N-1);j=j+1){
      if(state[i][j] == 1){
	fprintf(shoots, " +1");
      }else{
	fprintf(shoots, " %d",state[i][j]);
      }
    }
  }
}

float rxnc(int n, int s){
  float r0, aN, aS;
  float r, fn, fs, rn, rs;

  r = (float)n;
  /*
  rn = sqrt(((float)n)/(4.0*3.1416));
  rs = ((float)s)/(2.0*3.1416);
  r = -2.995+6.703*(rn-0.936)/(4.513)+0.669*(rs-2.546)/(20.372); 

  SCANNED INPUT
 1 0.936 5.448 4.513 
 2 2.546 22.918 20.372 
  REDUCED VARIABLES: Z = (Q-Qmin)/(Qmax-Qmin) 

        Z IN [0, 1]: Q = Z(Qmax-Qmin)+Qmin


  BIC = LN(N)/2 = 4.300

  PREPARE AND COMPILE MAXIMIZATION ROUTINE FOR M=1
 rxncoor 1 -2162.210016 -3.001 7.290
 rxncoor 2 -2351.942844 -2.366 6.841
 
 best rxncoor 1 -2162.210016 -3.001 7.290

 PREPARE AND COMPILE MAXIMIZATION ROUTINE 
 rxncoor 1 2 -2160.412158 -2.995 6.703 0.669
 
 best rxncoor 1 2 -2160.412158 -2.995 6.703 0.669

  */
  return r;
}

int mod(int i){
  /* assume base N */
  int k;
  k = i;
  while( k<0 ){
    k = k + N;
  }
  k = k%N;
  return k;
}

void edge_of_cluster(int big[1+N*N][3], int edge[1+N*N][3]){
  
  int i, j, k, im, ip, jm, jp, km, kp; 
  int ne, nk, ik, inthere, inbig;
  
  reset_cluster(edge);
  nk = big[0][0];
  ne = 0;
  for(ik=1;ik<=nk;ik=ik+1){

    i = big[ik][1];
    j = big[ik][2];

    ip = mod(i+1);
    im = mod(i-1);
    jp = mod(j+1);
    jm = mod(j-1);

    /* TEST EACH NEIGHBOR - ADD TO EDGE */
    inthere = element_of_cluster( ip, j, edge);
    inbig = element_of_cluster(ip, j, big);
    if( inthere == 0  &&  inbig == 0 ){
      ne = ne + 1;
      edge[0][0] = ne;
      edge[ne][1] = ip;
      edge[ne][2] = j;
    }
    inthere = element_of_cluster( im, j, edge);
    inbig = element_of_cluster(im, j, big);
    if( inthere == 0  &&  inbig == 0 ){
      ne = ne + 1;
      edge[0][0] = ne;
      edge[ne][1] = im;
      edge[ne][2] = j;
    }
    inthere = element_of_cluster( i, jp, edge);
    inbig = element_of_cluster(i, jp, big);
    if( inthere == 0  &&  inbig == 0 ){
      ne = ne + 1;
      edge[0][0] = ne;
      edge[ne][1] = i;
      edge[ne][2] = jp;
    }
    inthere = element_of_cluster( i, jm, edge);
    inbig = element_of_cluster(i, jm, big);
    if( inthere == 0  &&  inbig == 0 ){
      ne = ne + 1;
      edge[0][0] = ne;
      edge[ne][1] = i;
      edge[ne][2] = jm;
    }
  }
}

void copy_cluster(int big[1+N*N][3], int nbig[1+N*N][3]){
  int i, j;
  for(i=0;i<=(N*N);i=i+1){
    for(j=0;j<=2;j=j+1){
      nbig[i][j] = big[i][j];
    }
  }
}

void biased_mc_step(float mu, float sig,
		    int state[N][N], int big[1+N*N][3], 
		    int edge[1+N*N][3]){
  
  int i, j, k, accept, kk, nstate[N][N];
  float drE, dE, BIAS, kr;
  int nbig[1+N*N][3], nedge[1+N*N][3];
  int n, s, inthere;
  int nn, ns;
  float r, nr, rE, nrE;

  /* kT=1 in this code, duh  */
  /* kr=100kT NOT ARBITRARY  */
  /* SYSTEM INDEP RXN COORD  */
  /* IS r=+/-0.2 at pB=.4/.6 */
  /* SAMPLE THIS pB WINDOW   */
  /* UMBRELLA IS .5 k r^2 w/ */
  /* k=100. U(r)=2kT CAPISCO */
  
  kr = 100.0;
  
  i = randomint(0, N-1);
  j = randomint(0, N-1);
  inthere = element_of_cluster(i, j, big);
  inthere = inthere + element_of_cluster(i, j, edge);
  if( inthere == 2 ){
        printf(" \n ERROR: STRONSO");
  }
  if( inthere == 1 ){
    s = perimeter(state, big);
    n = big[0][0];
    r = rxnc(n, s);
    rE = r*r*kr/2.0;
    copy_state(state, nstate);
    nstate[i][j] = -state[i][j];
    dE = e_change(mu, sig, i, j, state);
    copy_cluster(big, nbig);
    cluster_from_cluster(nstate, nbig);
    ns = perimeter(nstate, nbig);
    nn = nbig[0][0];
    nr = rxnc(nn, ns);
    nrE = nr*nr*kr/2.0;		
    dE = dE + nrE - rE;
    accept = metropolis_mc(dE);
    if( accept == 1 ){
      state[i][j] = -state[i][j];
      copy_cluster(nbig, big);
      edge_of_cluster(big, edge);
    }
  }
  if( inthere == 0 ){
    dE = e_change(mu, sig, i, j, state);
    accept = metropolis_mc(dE);
    if( accept == 1 ){
      state[i][j] = -state[i][j];
    }
  }
}

int metropolis_mc(float d){
  int accept;
  float r;
  accept = 0;
  if(d < 0.0){
    accept = 1;
  }else{
    r = ran01();
    /* there is finite probability of generating zero */
    /* if exp(-d) < finite probability could burp an  */
    /* occaisonal accidental high action trajectory   */
    /* fixed by shifting ran01 by 0.5 smallest width  */
    if(r < exp(-d)){
      accept = 1;
    }
  }
  return accept;
}

float metropolis(float d){
  float met;
  if(d <= 0.0){
    met = 1.0;
  }else{
    met = exp(-d);
  }
  return met;
}

void copy_state(int state[N][N], int newstate[N][N]){
  int i, j, k;
  for(i=0;i<=(N-1);i=i+1){
    for(j=0;j<=(N-1);j=j+1){
	newstate[i][j] = state[i][j];
    }
  }
}

float energy(float hh, float JJ, int state[N][N]){
  float E;
  int i, j, k, s, nB;
  nB = 0;
  E=0.0;
  for(i=0;i<=N-1;i=i+1){
    for(j=0;j<=N-1;j=j+1){
      s = state[i][j];
      E = E - hh*((float)s);
      E = E - JJ*((float)(s*state[mod(i+1)][j])); 
      E = E - JJ*((float)(s*state[i][mod(j+1)]));
    }
  }
  return E;
}

float e_change(float hh, float JJ, 
	       int i, int j, int state[N][N]){
  float dE;
  int s;
  s = state[i][j];
  dE = 2.0*hh*(float)s;
  dE = dE + 2.0*JJ*(float)(s*state[i][mod(j-1)] + s*state[i][mod(j+1)]);
  dE = dE + 2.0*JJ*(float)(s*state[mod(i-1)][j] + s*state[mod(i+1)][j]);
  return dE;
}

int randomint(int a, int b){
  int diff, aa;
  diff = b - a + 1;
  if(a < 0){
    printf("\n CAREFUL: C's % is not ALGEBRA's MOD");
  }
  aa = rand()%diff;
  return (a+aa);
}

float ran01(void){
  int k, kmax;
  float kf;
  kmax = 1000000;
  k = randomint(1, kmax);
  kf = ((float)k+0.5)/((float)kmax);
  return kf;
}

void find_big_cluster(int state[N][N], int big[1+N*N][3]){

  int i, j, k;
  int little[1+N*N][3];
  int ip, im, jp, jm, ii, jj, inthere;
  int c, km, kp, kk, nc, ic, error_mark;
  FILE *errfile;

  reset_cluster(big);
  reset_cluster(little);
  for(i=0;i<=(N-1);i=i+1){
    for(j=0;j<=(N-1);j=j+1){
      inthere = 1;
      if( state[i][j] == 1 ){
	/* SEE IF (i,j) IS IN EITHER CLUSTER */
	inthere = element_of_cluster(i, j, big);
	inthere = inthere + element_of_cluster(i, j, little);
	if( inthere == 2 ){
	  printf("\n (%d,%d) IN 2 CLUSTERS->BUG !!! ", i, j);
	  error_mark = 1;
	}
      }
      if(inthere == 0){
	reset_cluster(little);
	/* BUILD LITTLE CLUSTER AROUND (i,j) */
	nc = 1;
	little[1][1] = i;
	little[1][2] = j;
	little[0][0] = 1;
	
	for(ic=1;ic<=nc;ic=ic+1){
	  /* NEIGHBORS OF ic ELEMENT */
	  ii = little[ic][1];
	  jj = little[ic][2];
	  ip = mod(ii+1);
	  im = mod(ii-1);
	  jp = mod(jj+1);
	  jm = mod(jj-1);
	  
	  /* TEST EACH NEIGHBOR - ADD TO CLUSTER */
	  if( state[ii][jp] == 1 ){
	    inthere = element_of_cluster( ii, jp, little);
	    if( inthere == 0 ){
	      nc = nc + 1;
	      little[0][0] = nc;
	      little[nc][1] = ii;
	      little[nc][2] = jp;
	    }
	  }
	  if( state[ii][jm] == 1 ){
	    inthere = element_of_cluster( ii, jm, little);
	    if( inthere == 0 ){
	      nc = nc + 1;
	      little[0][0] = nc;
	      little[nc][1] = ii;
	      little[nc][2] = jm;
	    }
	  }
	  if( state[im][jj] == 1 ){
	    inthere = element_of_cluster( im, jj, little);
	    if( inthere == 0 ){
	      nc = nc + 1;
	      little[0][0] = nc;
	      little[nc][1] = im;
	      little[nc][2] = jj;
	    }
	  }
	  if( state[ip][jj] == 1 ){
	    inthere = element_of_cluster( ip, jj, little);
	    if( inthere == 0 ){
	      nc = nc + 1;
	      little[0][0] = nc;
	      little[nc][1] = ip;
	      little[nc][2] = jj;
	    }
	  }
	}
      }
      if( little[0][0] > big[0][0] ){
	for(c=0;c<=(N*N);c=c+1){
	  for(kp=0;kp<=2;kp=kp+1){
	    big[c][kp] = little[c][kp];
	  }
	}
      }
      reset_cluster(little);
    }
  }
}

void cluster_from_cluster(int state[N][N], int big[1+N*N][3]){

  int i, j, k, nk, ik;
  int spawn[1+N*N][3], little[1+N*N][3];
  int ip, im, jp, jm, ii, jj, inthere;
  int c, km, kp, kk, nc, ic, error_mark;
  FILE *errfile;

  reset_cluster(spawn);
  reset_cluster(little);
  nk = big[0][0];
  for(ik=1;ik<=nk;ik=ik+1){
    i = big[ik][1];
    j = big[ik][2];
    inthere = 1;
    if( state[i][j] == 1 ){
      /* SEE IF (i,j) IS IN EITHER CLUSTER */
      inthere = element_of_cluster(i, j, spawn);
      inthere = inthere + element_of_cluster(i, j, little);
      if( inthere == 2 ){
	printf("\n (%d,%d) IN 2 CLUSTERS->BUG !!! ", i, j);
	error_mark = 1;
      }
    }
    if(inthere == 0){
      reset_cluster(little);
      /* BUILD LITTLE CLUSTER AROUND (i,j) */
      nc = 1;
      little[1][1] = i;
      little[1][2] = j;
      little[0][0] = 1;
      
      for(ic=1;ic<=nc;ic=ic+1){
	/* NEIGHBORS OF ic ELEMENT */
	ii = little[ic][1];
	jj = little[ic][2];
	ip = mod(ii+1);
	im = mod(ii-1);
	jp = mod(jj+1);
	jm = mod(jj-1);
	
	/* TEST EACH NEIGHBOR - ADD TO CLUSTER */
	if( state[ii][jp] == 1 ){
	  inthere = element_of_cluster( ii, jp, little);
	  if( inthere == 0 ){
	    nc = nc + 1;
	    little[0][0] = nc;
	    little[nc][1] = ii;
	    little[nc][2] = jp;
	  }
	}
	if( state[ii][jm] == 1 ){
	  inthere = element_of_cluster( ii, jm, little);
	  if( inthere == 0 ){
	    nc = nc + 1;
	    little[0][0] = nc;
	    little[nc][1] = ii;
	    little[nc][2] = jm;
	  }
	}
	if( state[im][jj] == 1 ){
	  inthere = element_of_cluster( im, jj, little);
	  if( inthere == 0 ){
	    nc = nc + 1;
	    little[0][0] = nc;
	    little[nc][1] = im;
	    little[nc][2] = jj;
	  }
	}
	if( state[ip][jj] == 1 ){
	  inthere = element_of_cluster( ip, jj, little);
	  if( inthere == 0 ){
	    nc = nc + 1;
	    little[0][0] = nc;
	    little[nc][1] = ip;
	    little[nc][2] = jj;
	  }
	}
      } 
    }
    if( little[0][0] > spawn[0][0] ){
      for(c=0;c<=(N*N);c=c+1){
	for(kp=0;kp<=2;kp=kp+1){
	  spawn[c][kp] = little[c][kp];
	}
      }
    }
    reset_cluster(little);
  }
  for(c=0;c<=(N*N);c=c+1){
    for(kp=0;kp<=2;kp=kp+1){
      big[c][kp] = spawn[c][kp];
    }
  }
}


int element_of_cluster(int i, int j, int big[1+N*N][3]){
  int nk, kk, yes;
  nk = big[0][0];
  yes = 0;
  for(kk=1;kk<=nk;kk=kk+1){
    if( i==big[kk][1] ){
      if( j==big[kk][2] ){ 
	  yes = 1;
      }
    }
  }
  return yes;
}

int perimeter(int state[N][N], int big[1+N*N][3]){
  int ii, jj, im, ip, jm, jp, km, kp;
  int surf, nk, kk, k;
  float test;
  surf = 0;
  nk = big[0][0];
  for(k=1;k<=nk;k=k+1){
    ii = big[k][1];
    jj = big[k][2];
    ip = mod(ii+1);
    im = mod(ii-1);
    jp = mod(jj+1);
    jm = mod(jj-1);
    if( state[ii][jp] == -1 ){
      surf = surf + 1;
    }
    if( state[ii][jm] == -1 ){
      surf = surf + 1;
    }
    if( state[ip][jj] == -1 ){
      surf = surf + 1;
    }
    if( state[im][jj] == -1 ){
      surf = surf + 1;
    }
  }
  return surf;
}

void reset_cluster(int cluster[1+N*N][3]){
  int i, j, k;
  for(j=0;j<=(N*N);j=j+1){
    for(k=0;k<=3;k=k+1){
      cluster[j][k] = 0;
    }
  }
}



