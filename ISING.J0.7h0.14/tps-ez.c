/*************************************************************************/
/*************  ISING MODEL OF MOLECULAR CRYSTAL NUCLEATION  *************/
/*************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define N 32
#define T N*N*N*N
#define EQU 20    /*  demo: run more than this for a real calculation !  */
#define TPS 100   /*  demo: run more than this for a real calculation !  */
#define J 0.70
#define h 0.14
#define redgeA 10.0
#define redgeB 100.0

#define DEBUG 1

/*************************************************************************/
/**********************  Function Declarations  **************************/
/*************************************************************************/

#include "./lattice2D-define.c"

/*************************************************************************/
/*****************************  MAIN  ************************************/
/*************************************************************************/

main(){

  int i, j, k, acc, count, seed;
  int x[4][N][N], xnew[4][N][N];
  int n[4], s[4], m[4], big[1+N*N][3];
  int hA0, hAT, hB0, hBT;
  float r[4];
  FILE *data, *shoots;

  system("date");
  seed = time(NULL);
  srand(seed);
  printf("\n N=%d h/kT=%.2f J/kT=%.2f", N, h, J);
  fflush(stdout);
  for(i=0;i<N;i=i+1){
    for(j=0;j<N;j=j+1){
      x[1][i][j] = -1;
      x[2][i][j] = -1;
    }
  }
  for(i=16;i<=22;i=i+1){
    for(j=16;j<=22;j=j+1){
      x[1][i][j] = 1;
      x[2][i][j] = 1;
    }
  }

  count = 0;
  while(count < EQU){
    acc = 0;
    k = randomint(1,2);
    copy_state(x[k], xnew[1]);
    find_big_cluster(xnew[1], big);
    n[1] = big[0][0];
    s[1] = perimeter(xnew[1], big);
    printf("\n  shoot=(%d %d) r=%f ", n[1], s[1], rxnc(n[1], s[1]));
    if( hfn(n[1], s[1]) == 'A'){
      hA0 = hAT = 1;
      hB0 = hBT = 0;
    }else{
      if( hfn(n[1], s[1]) == 'B'){
	hA0 = hAT = 0;
	hB0 = hBT = 1;
      }else{
	traj_segment(h, J, T, xnew[1], xnew[0]);
	find_big_cluster(xnew[0], big);
	n[0] = big[0][0];
	s[0] = perimeter(xnew[0], big);
	printf(" %c <- (%d %d)", hfn(n[0], s[0]), n[1], s[1]);
	if( hfn(n[0], s[0]) == 'A' ){
	  hA0 = 1;
	  hB0 = 0;
	}
	if( hfn(n[0], s[0]) == 'B' ){
	  hA0 = 0;
	  hB0 = 1;
	}
	if( hfn(n[0], s[0]) == 'I' ){
	  hA0 = hB0 = 0;
	}
	traj_segment(h, J, T/N, xnew[1], xnew[2]);	
	find_big_cluster(xnew[2], big);
	n[2] = big[0][0];
	s[2] = perimeter(xnew[2], big);
	if( hfn(n[2], s[2]) == 'A'){
	  hAT = 1;
	  hBT = 0;
	  printf(" A <- (%d %d)", n[1], s[1]);
	}else{
	  if( hfn(n[2], s[2]) == 'B'){
	    hAT = 0;
	    hBT = 1;
	    printf(" B <- (%d %d)", n[1], s[1]);
	  }else{
	    traj_segment(h, J, T, xnew[2], xnew[3]);
	    find_big_cluster(xnew[3], big);
	    n[3] = big[0][0];
	    s[3] = perimeter(xnew[3], big);
	    printf(" %c <- (%d %d)", hfn(n[3], n[3]), n[2], s[2]);
	    if( hfn(n[3], s[3]) == 'A'){
	      hAT = 1;
	      hBT = 0;
	    }
	    if( hfn(n[3], s[3]) == 'B'){
	      hAT = 0;
	      hBT = 1;
	    }
	    if( hfn(n[3], s[3]) == 'I'){
	      hAT = hBT = 0;
	    }
	  }
	}
      }
    }
    if( (hA0*hBT + hB0*hAT) == 1 ){
      acc = 1;
      copy_state(xnew[0], x[0]);
      copy_state(xnew[1], x[1]);
      copy_state(xnew[2], x[2]);
      copy_state(xnew[3], x[3]);
      count = count + 1;
      printf(" acc");
    }
    if( ((hA0+hB0) + (hAT+hBT)) < 2 ){
      printf(" inc");
    }
    if( ((hA0*hAT) + (hB0*hBT)) == 1 ){
      printf(" rej");
    }
  }


  data = fopen("data.txt","w");
  shoots = fopen("shootpts.txt", "w");
  count = 0;
  while(count < TPS){
    acc = 0;
    k = randomint(1,2);
    copy_state(x[k], xnew[1]);
    find_big_cluster(xnew[1], big);
    n[1] = big[0][0];
    s[1] = perimeter(xnew[1], big);
    printf("\n  shoot = (%d %d) r=%f", n[1], s[1], rxnc(n[1], s[1]));
    print_state(shoots, xnew[1]);
    if( hfn(n[1], s[1]) == 'A'){
      hA0 = hAT = 1;
      hB0 = hBT = 0;
      /* dont even bother running trajectory */
    }else{
      if( hfn(n[1], s[1]) == 'B'){
        hA0 = hAT = 0;
        hB0 = hBT = 1;
	/* dont even bother running trajectory */
      }else{
        traj_segment(h, J, T, xnew[1], xnew[0]);
        find_big_cluster(xnew[0], big);
        n[0] = big[0][0];
        s[0] = perimeter(xnew[0], big);
	fprintf(data, "\n %c <- %d %d", hfn(n[0], s[0]), n[1], s[1]);
        if( hfn(n[0], s[0]) == 'A' ){
          hA0 = 1;
          hB0 = 0;
        }
        if( hfn(n[0], s[0]) == 'B' ){
          hA0 = 0;
          hB0 = 1;
        }
	if( hfn(n[0], s[0]) == 'I' ){
	  hA0 = hB0 = 0;
	  /* inconclusive */
	}
        traj_segment(h, J, T/N, xnew[1], xnew[2]);
	/* this is the little short trajectory */
        find_big_cluster(xnew[2], big);
        n[2] = big[0][0];
        s[2] = perimeter(xnew[2], big);
        if( hfn(n[2], s[2]) == 'A'){
	  /* dont bother running the rest */
          hAT = 1;
          hBT = 0;
	  fprintf(data, "\n A <- %d %d", n[1], s[1]);
        }else{
          if( hfn(n[2], s[2]) == 'B'){
	    /* dont bother running the rest */
            hAT = 0;
            hBT = 1;
	    fprintf(data, "\n B <- %d %d", n[1], s[1]);
          }else{
            traj_segment(h, J, T, xnew[2], xnew[3]);
            find_big_cluster(xnew[3], big);
            n[3] = big[0][0];
            s[3] = perimeter(xnew[3], big);
	    fprintf(data, "\n %c <- %d %d", hfn(n[3], n[3]), n[2], s[2]);
            if( hfn(n[3], s[3]) == 'A'){
              hAT = 1;
              hBT = 0;
            }
            if( hfn(n[3], s[3]) == 'B'){
              hAT = 0;
              hBT = 1;
	    }
	    if( hfn(n[3], s[3]) == 'I'){
	      hAT = hBT = 0;
	    }
          }
        }
      }
    }
    if( (hA0*hBT + hB0*hAT) == 1 ){
      acc = 1;
      copy_state(xnew[0], x[0]);
      copy_state(xnew[1], x[1]);
      copy_state(xnew[2], x[2]);
      copy_state(xnew[3], x[3]);
      count = count + 1;
      printf(" acc");
    }
    if( ((hA0+hB0) + (hAT+hBT)) < 2 ){
      printf(" inc");
    }
    if( ((hA0*hAT) + (hB0*hBT)) == 1 ){
      printf(" rej");
    }
  }


  fclose(shoots);
  fclose(data);
  printf("\n");

}




