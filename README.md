PATH SAMPLING SECTION: tps-ez.c

first define basins A and B by setting the redgeA and redgeB variables.
for the ising model, i used threshold sizes of the largest cluster of 
"up" spins in the system.  (cluster size is a pretty good coordinate,
but it doesn't have to be good - the important thing is to ensure that 
states in your A-basin "never" go to B, and that states in the B-basin
"never" go to A.  ideally you want each basin just large enough to 
contain the states visited by a typical equilibrium trajectory in that 
basin. 

AandB.c actually runs a little spin flip trajectory started in the 
metastable (A) state to help identify a reasonable boundary "redgeA".  
for the ising model, redgeB is a little more tricky to define.  You
could use the standard scatter around the stable "all up" state, but
that would require trajectories long enough to include growth from 
spin up in the critical nucleus (small) to the whole box.  You only 
need a size for redgeB that is sufficiently post-critical to safely 
assume the nucleus will continue to grow.  you could adapt AandB.c
to estimate that size too. (shoot from clusters of a known size and
see how big they have to be for all trajectories to result in growth)
for this example the value redgeB=100 is good.      

for running the "aimless shooting" version of transition path sampling,
you'll need to set the duration of each trajectory (T + dT + T) where
the components are for a backward shoot, a short section between two 
shooting points, and a forward shoot.  For this example, dT=T/N where
T = N*N*N*N and N is the lattice size: NxN = 32x32 = 1024 spins.  

(by the way, the unit of time is a sweep = N*N spin flip attempts)

J and h are the usual ising model parameters, here both in units of kT.  

LIKELIHOOD MAXIMIZATION SECTION: baba2.c

after you have run the tps-ez.c code, you will see output files data.txt
and shootpts.txt.  data.txt is the important one, but its wise to save
these (with their outcomes unlike the sloppy work i did here!) they will
be useful if the initial coordinates you analyze are not sufficient to 
build a good coordinate.  (see papers)
  
separate the data.txt file into A outcomes and B outcomes by running the
commands:
   "grep A data.txt > A-list.txt"
   "grep B data.txt > B-list.txt"
then (i keep the tps and lmax calculations in separate folders) see how
long the files A-list.txt and B-list.txt are:
   "wc A-list.txt"   
   "wc B-list.txt"
wc gives the file length in lines followed by some other junk...

NA and NB in baba2.c are set to the number of data points in the A and B
files respectively.  (i might have set them to n-1 if there is a blank 
line at the end?) 
     
M is the number of variables in the data list.  You can always change these 
to get new (but related variables).  for example, I took the variable n and 
computed sqrt(n/(4*pi)) to get a "radius".  similarly, I took s (perimeter)
and computed s/(2*pi) to get another measure of the radius.  

baba2.c will convert all of your coordinates to reduced variables on a 
scale from 0 (smallest value sampled at a shooting point) to 1 (largest 
value sampled at a shooting point).  This way, the optimization to get 
"alphas" can be handled by a generic maximization routine.  You;ll see 
output like this:

 ---------------------------------------------------------
 Wed Apr 23 16:34:01 EDT 2008

  SCANNED INPUT
 1 0.892 2.793 1.901 
 2 2.546 10.186 7.639 
  REDUCED VARIABLES: Z = (Q-Qmin)/(Qmax-Qmin) 

        Z IN [0, 1]: Q = Z(Qmax-Qmin)+Qmin

  BIC = LN(N)/2 = 3.403

  PREPARE AND COMPILE MAXIMIZATION ROUTINE FOR M=1
 rxncoor 1 -197.087654 -2.386 5.333
 rxncoor 2 -267.584196 -2.272 6.202
 
 best rxncoor 1 -197.087654 -2.386 5.333

 PREPARE AND COMPILE MAXIMIZATION ROUTINE 
 rxncoor 1 2 -196.765972 -2.428 4.919 0.648
 
 best rxncoor 1 2 -196.765972 -2.428 4.919 0.648

 ---------------------------------------------------------

 SOME COMMENTS ON INTERPRETING OUTPUT:

 1) the BIC suggests that the best 2D reaction coordinate is
    not a significant improvement over the best 1D rxn coordinate. 
    i.e. (-196.766+197.088) < 3.403
    so we should use the 1st coordinate r = sqrt(n/(4*pi))
 
 2) the shooting point with the samllest r has rmin = 0.892 (n=10)
    the shooting point with the largest r has rmax = 2.793 (n=98)
    the values of r span an interval of width rmax-rmin = 1.901
 
 3) the reduced variable corresponding to r is
    
    z = (r-rmin)/(rmax-rmin) 

    and the reaction coordinate in that variable is 

    rc = 5.333*z-2.386

 4) the transition state (critical nucleus) is 

    rc = 0  =>  z* = 0.4474  =>  r* = 0.4474*1.901+0.892 = 1.7425

    or                           n* = 38 



